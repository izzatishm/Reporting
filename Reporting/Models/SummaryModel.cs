﻿using Reporting.Models.Entities;
using Reporting.Models.Objects;
using Reporting.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Reporting.Models
{
    public class SummaryModel
    {
        public IEnumerable<SummaryTransactions> SummaryTransactions(SummaryViewModel filter)
        {
            var StartDate = "2019-01-01 00:00:01";
            var EndDate = "2019-12-31 23:59:59";
           
            using (var ctx = new ReportingContext())
            {

                var result = from A in ctx.tPurchaseOrders
                             where A.TransferDate.CompareTo(StartDate) >= 0
                             && A.TransferDate.CompareTo(EndDate) < 0
                             select A;

                if (filter.Summary != null)
                {
                    if (filter.Summary.CompanyRemID != null)
                    {
                        result = result.Where(x => x.CompanyRemID.Equals(filter.Summary.CompanyRemID));
                    }

                    if (filter.Summary.Month != null)
                    {
                        result = result.Where(x => x.TransferDate.Substring(5, 2).Equals(filter.Summary.Month));
                    }

                    if (filter.Summary.Year != null)
                    {
                        result = result.Where(x => x.TransferDate.Substring(0, 4).Equals(filter.Summary.Year));
                    }
                }

                var FinalResult = from A in result
                                 group A by A.TransferDate.Substring(5,2) into g
                                 orderby g.Key ascending
                                 select new SummaryTransactions
                                 {
                                     Month = g.Key,
                                     TotalCount = g.Count().ToString(),
                                     TotalAmount = g.Sum(x => x.TransferAmount)
                                 };

                List<SummaryTransactions> SummaryCounts = FinalResult.ToList();
                return SummaryCounts;
            }
        }
    }
}