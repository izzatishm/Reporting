﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Reporting.Models.Objects
{
    public class AccountInfoTransactions
    {
        public string CreateDateTime { get; set; }
        public string Site1CompanyRemID { get; set; }
        public string LoginUsername { get; set; }
        public bool? AccountVerified { get; set; }
        public string CompanyRemID { get; set; }
    }
}