﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Reporting.Models.Objects
{
    public class SummaryTransactions
    {
        public string TotalCount { get; set; }
        public decimal? TotalAmount { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public string CompanyRemID { get; set; }
        public decimal? TransferAmount { get; set; }
        public string TransferDate { get; set; }
    }
}