﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Reporting.Models.Objects
{
    public class SuccessRateTransactions
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string CompanyRemID { get; set; }
        public string TransferDate { get; set; }
        public decimal SuccessRate { get; set; }
        public decimal? FailureRate { get; set; }
        public int TotalCount { get; set; }
        public string Status { get; set; }
        public string Day { get; set; }
        public string Month { get; set; }
    }
}