﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Reporting.Models.Objects
{
    public class ObjTransactions
    {
        public string TransferDate { get; set; }
        public string TransferHour { get; set; }
        public string TransferMinute { get; set; }
        public string CompanyRemID { get; set; }
        public string RemID { get; set; }
        public string CartID { get; set; }
        public string TransferCurrency { get; set; }
        public string TransferAmount { get; set; }
        public string Method { get; set; }
        public string Status { get; set; }


    }
}