﻿using Reporting.Models.Entities;
using Reporting.Models.Objects;
using Reporting.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Reporting.Models
{
    public class ReportingModel
    {
        public IEnumerable<tPurchaseOrder> ListTransaction(ListViewModel filter)
        {
            var StartDate = "2019-01-01 00:00:01";
            var EndDate = "2019-12-31 23:59:59";

            using (var ctx = new ReportingContext())
            {
                var result = from A in ctx.tPurchaseOrders
                             where A.TransferDate.CompareTo(StartDate) >=0
                             && A.TransferDate.CompareTo(EndDate) < 0
                             select A;

                if (filter.TransactionHistory != null)
                {
                    if(filter.TransactionHistory.TransferDate != null)
                    {
                        result = result.Where(x => x.TransferDate.Substring(0,10).Equals(filter.TransactionHistory.TransferDate));
                    }

                    if(filter.TransactionHistory.TransferHour != null)
                    {
                        result = result.Where(x => x.TransferDate.Substring(11,2).Equals(filter.TransactionHistory.TransferHour));
                    }

                    if (filter.TransactionHistory.TransferMinute != null)
                    {
                        result = result.Where(x => x.TransferDate.Substring(14, 2).Equals(filter.TransactionHistory.TransferMinute));
                    }

                    if (filter.TransactionHistory.CompanyRemID != null)
                    {
                        result = result.Where(x => x.CompanyRemID.Equals(filter.TransactionHistory.CompanyRemID));
                    }

                    if (filter.TransactionHistory.RemID != null)
                    {
                        result = result.Where(x => x.RemID.Equals(filter.TransactionHistory.RemID));
                    }

                    if (filter.TransactionHistory.CartID != null)
                    {
                        result = result.Where(x => x.CartID.Equals(filter.TransactionHistory.CartID));
                    }

                    if (filter.TransactionHistory.Status != null)
                    {
                        result = result.Where(x => x.Status.Equals(filter.TransactionHistory.Status));
                    }

                    if (filter.TransactionHistory.Method != null)
                    {
                        result = result.Where(x => x.Method.Equals(filter.TransactionHistory.Method));
                    }
                }

                //include the type of the List
                List<tPurchaseOrder> ListTransactions = result.ToList();
                //return the list
                return ListTransactions;
            }
        }
    }
}