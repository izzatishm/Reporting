namespace Reporting.Models.Entities
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class ReportingContext : DbContext
    {
        public ReportingContext()
            : base("name=ReportingContext1")
        {
        }

        public virtual DbSet<tMembersAccInfo> tMembersAccInfoes { get; set; }
        public virtual DbSet<tMembersAccStatu> tMembersAccStatus { get; set; }
        public virtual DbSet<tMembersInfo> tMembersInfoes { get; set; }
        public virtual DbSet<tPurchaseOrder> tPurchaseOrders { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<tMembersAccInfo>()
                .Property(e => e.SecretQuestion)
                .IsUnicode(false);

            modelBuilder.Entity<tMembersAccInfo>()
                .Property(e => e.SecretAnswer)
                .IsUnicode(false);

            modelBuilder.Entity<tMembersAccInfo>()
                .Property(e => e.ReservationAmount)
                .HasPrecision(18, 4);

            modelBuilder.Entity<tMembersAccInfo>()
                .Property(e => e.CurrentBalance)
                .HasPrecision(18, 4);

            modelBuilder.Entity<tMembersAccInfo>()
                .Property(e => e.AvailableBalance)
                .HasPrecision(18, 4);

            modelBuilder.Entity<tMembersAccInfo>()
                .Property(e => e.PWBalance)
                .HasPrecision(18, 4);

            modelBuilder.Entity<tMembersAccInfo>()
                .Property(e => e.PurchasePVBalance)
                .HasPrecision(18, 4);

            modelBuilder.Entity<tMembersAccInfo>()
                .Property(e => e.PVPoint)
                .HasPrecision(18, 4);

            modelBuilder.Entity<tMembersAccInfo>()
                .Property(e => e.PVPointX)
                .HasPrecision(18, 4);

            modelBuilder.Entity<tMembersAccInfo>()
                .Property(e => e.LeftPVPoint)
                .HasPrecision(18, 4);

            modelBuilder.Entity<tMembersAccInfo>()
                .Property(e => e.MiddlePVPoint)
                .HasPrecision(18, 4);

            modelBuilder.Entity<tMembersAccInfo>()
                .Property(e => e.RightPVPoint)
                .HasPrecision(18, 4);

            modelBuilder.Entity<tMembersAccInfo>()
                .Property(e => e.LeftPVPointX)
                .HasPrecision(18, 4);

            modelBuilder.Entity<tMembersAccInfo>()
                .Property(e => e.MiddlePVPointX)
                .HasPrecision(18, 4);

            modelBuilder.Entity<tMembersAccInfo>()
                .Property(e => e.RightPVPointX)
                .HasPrecision(18, 4);

            modelBuilder.Entity<tMembersAccInfo>()
                .Property(e => e.StockValue)
                .HasPrecision(18, 4);

            modelBuilder.Entity<tMembersAccInfo>()
                .Property(e => e.LeftPVPointCF)
                .HasPrecision(18, 4);

            modelBuilder.Entity<tMembersAccInfo>()
                .Property(e => e.MiddlePVPointCF)
                .HasPrecision(18, 4);

            modelBuilder.Entity<tMembersAccInfo>()
                .Property(e => e.RightPVPointCF)
                .HasPrecision(18, 4);

            modelBuilder.Entity<tMembersAccInfo>()
                .Property(e => e.LeftPVAdj)
                .HasPrecision(18, 4);

            modelBuilder.Entity<tMembersAccInfo>()
                .Property(e => e.MiddlePVAdj)
                .HasPrecision(18, 4);

            modelBuilder.Entity<tMembersAccInfo>()
                .Property(e => e.RightPVAdj)
                .HasPrecision(18, 4);

            modelBuilder.Entity<tMembersAccInfo>()
                .Property(e => e.LeftPVPointCFY)
                .HasPrecision(18, 4);

            modelBuilder.Entity<tMembersAccInfo>()
                .Property(e => e.MiddlePVPointCFY)
                .HasPrecision(18, 4);

            modelBuilder.Entity<tMembersAccInfo>()
                .Property(e => e.RightPVPointCFY)
                .HasPrecision(18, 4);

            modelBuilder.Entity<tMembersAccInfo>()
                .Property(e => e.LeftNewSales)
                .HasPrecision(18, 4);

            modelBuilder.Entity<tMembersAccInfo>()
                .Property(e => e.MiddleNewSales)
                .HasPrecision(18, 4);

            modelBuilder.Entity<tMembersAccInfo>()
                .Property(e => e.RightNewSales)
                .HasPrecision(18, 4);

            modelBuilder.Entity<tMembersAccInfo>()
                .Property(e => e.AffiliateHits)
                .HasPrecision(18, 4);

            modelBuilder.Entity<tMembersAccInfo>()
                .Property(e => e.Bonus1)
                .HasPrecision(18, 4);

            modelBuilder.Entity<tMembersAccInfo>()
                .Property(e => e.Bonus2)
                .HasPrecision(18, 4);

            modelBuilder.Entity<tMembersAccInfo>()
                .Property(e => e.Bonus3)
                .HasPrecision(18, 4);

            modelBuilder.Entity<tMembersAccInfo>()
                .Property(e => e.SMSWalletBalance)
                .HasPrecision(18, 4);

            modelBuilder.Entity<tMembersAccInfo>()
                .Property(e => e.MaintWallet)
                .HasPrecision(18, 4);

            modelBuilder.Entity<tMembersAccInfo>()
                .Property(e => e.ServiceFeeBalance)
                .HasPrecision(18, 4);

            modelBuilder.Entity<tMembersAccStatu>()
                .Property(e => e.ID)
                .HasPrecision(18, 0);

            modelBuilder.Entity<tMembersAccStatu>()
                .Property(e => e.LoginCount)
                .HasPrecision(18, 0);

            modelBuilder.Entity<tMembersInfo>()
                .Property(e => e.ID)
                .HasPrecision(18, 0);

            modelBuilder.Entity<tMembersInfo>()
                .Property(e => e.WelcomeMessage)
                .IsUnicode(false);

            modelBuilder.Entity<tMembersInfo>()
                .Property(e => e.SettlementPercentage)
                .HasPrecision(6, 3);

            modelBuilder.Entity<tPurchaseOrder>()
                .Property(e => e.ID)
                .HasPrecision(18, 0);

            modelBuilder.Entity<tPurchaseOrder>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<tPurchaseOrder>()
                .Property(e => e.Remarks)
                .IsUnicode(false);

            modelBuilder.Entity<tPurchaseOrder>()
                .Property(e => e.SettlementFXRate)
                .HasPrecision(18, 8);

            modelBuilder.Entity<tPurchaseOrder>()
                .Property(e => e.Agent1Amount)
                .HasPrecision(18, 4);

            modelBuilder.Entity<tPurchaseOrder>()
                .Property(e => e.Agent2Amount)
                .HasPrecision(18, 4);

            modelBuilder.Entity<tPurchaseOrder>()
                .Property(e => e.MDRAmount)
                .HasPrecision(18, 4);

            modelBuilder.Entity<tPurchaseOrder>()
                .Property(e => e.GatewayAmount)
                .HasPrecision(18, 4);

            modelBuilder.Entity<tPurchaseOrder>()
                .Property(e => e.ProductPrice)
                .HasPrecision(18, 5);

            modelBuilder.Entity<tPurchaseOrder>()
                .Property(e => e.AmountAfterTax)
                .HasPrecision(18, 5);

            modelBuilder.Entity<tPurchaseOrder>()
                .Property(e => e.Balance)
                .HasPrecision(18, 5);

            modelBuilder.Entity<tPurchaseOrder>()
                .HasOptional(e => e.tPurchaseOrders1)
                .WithRequired(e => e.tPurchaseOrder1);
        }
    }
}
