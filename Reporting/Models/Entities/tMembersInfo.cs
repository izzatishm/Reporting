namespace Reporting.Models.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tMembersInfo")]
    public partial class tMembersInfo
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public decimal ID { get; set; }

        [StringLength(50)]
        public string CompanyRemID { get; set; }

        [StringLength(50)]
        public string Site1CompanyRemID { get; set; }

        [StringLength(50)]
        public string MemberID { get; set; }

        [StringLength(50)]
        public string MerchantKey { get; set; }

        [StringLength(255)]
        public string LoginUsername { get; set; }

        [StringLength(50)]
        public string FirstName { get; set; }

        [StringLength(50)]
        public string LastName { get; set; }

        public int? Gender { get; set; }

        [StringLength(50)]
        public string DOB { get; set; }

        [StringLength(255)]
        public string Password { get; set; }

        [StringLength(50)]
        public string DirectSponsorID { get; set; }

        [StringLength(50)]
        public string UL2ID { get; set; }

        [StringLength(50)]
        public string UL3ID { get; set; }

        [StringLength(50)]
        public string SCID { get; set; }

        [StringLength(50)]
        public string SCOID { get; set; }

        [StringLength(50)]
        public string SCO1ID { get; set; }

        [StringLength(50)]
        public string SCO2ID { get; set; }

        [StringLength(50)]
        public string SCO3ID { get; set; }

        [StringLength(50)]
        public string BranchID { get; set; }

        [StringLength(50)]
        public string UplineUpdate { get; set; }

        public string WelcomeMessage { get; set; }

        [StringLength(50)]
        public string CreateDateTime { get; set; }

        [StringLength(50)]
        public string CreateBy { get; set; }

        [StringLength(50)]
        public string UpdateDateTime { get; set; }

        [StringLength(50)]
        public string UpdateBy { get; set; }

        public bool? Deleted { get; set; }

        [StringLength(50)]
        public string LastLogin { get; set; }

        public bool? isStockist { get; set; }

        [StringLength(50)]
        public string StockistType { get; set; }

        [StringLength(50)]
        public string StockistJoinDate { get; set; }

        [StringLength(50)]
        public string ePin { get; set; }

        [StringLength(50)]
        public string SU1 { get; set; }

        [StringLength(50)]
        public string SU2 { get; set; }

        [StringLength(50)]
        public string SU3 { get; set; }

        [StringLength(50)]
        public string SU4 { get; set; }

        [StringLength(50)]
        public string SU5 { get; set; }

        [StringLength(50)]
        public string ProcessCode { get; set; }

        [StringLength(50)]
        public string RegisteredMobileNo { get; set; }

        [StringLength(50)]
        public string RegisteredMobileNoStatus { get; set; }

        public bool? IsFirstPurchase { get; set; }

        [StringLength(50)]
        public string GMID { get; set; }

        [StringLength(100)]
        public string Remarks { get; set; }

        [StringLength(50)]
        public string KeyVersion { get; set; }

        [StringLength(255)]
        public string KeyMessage { get; set; }

        [StringLength(50)]
        public string LastPassChangeDate { get; set; }

        public bool? Recurring { get; set; }

        [StringLength(50)]
        public string ParentID { get; set; }

        [StringLength(255)]
        public string HashPassword { get; set; }

        [StringLength(50)]
        public string SecretKey { get; set; }

        public bool? EnableTwoFA { get; set; }

        public bool? CredentialsSent { get; set; }

        [StringLength(50)]
        public string Agent1 { get; set; }

        [StringLength(50)]
        public string Agent2 { get; set; }

        public bool PaymentModule { get; set; }

        public bool PayoutModule { get; set; }

        [StringLength(50)]
        public string UserID { get; set; }

        public decimal? SettlementPercentage { get; set; }

        public string Doc { get; set; }

        [StringLength(50)]
        public string MemberType { get; set; }

        public bool AutoSettlement { get; set; }

        [StringLength(50)]
        public string zmkType { get; set; }
    }
}
