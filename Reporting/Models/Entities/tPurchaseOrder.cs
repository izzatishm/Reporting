namespace Reporting.Models.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tPurchaseOrder
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public decimal ID { get; set; }

        [StringLength(50)]
        public string RemID { get; set; }

        [StringLength(50)]
        public string CompanyRemID { get; set; }

        [StringLength(50)]
        public string MemberRemID { get; set; }

        [StringLength(50)]
        public string PayeeID { get; set; }

        public decimal? TotalPVs { get; set; }

        public decimal? BankCharges { get; set; }

        public decimal? TotalPaymentAmt { get; set; }

        public decimal? AdminClearanceAmt { get; set; }

        [StringLength(50)]
        public string Method { get; set; }

        [StringLength(50)]
        public string TransferCurrency { get; set; }

        public decimal? TransferAmount { get; set; }

        [StringLength(50)]
        public string ReferenceNo { get; set; }

        [StringLength(50)]
        public string TransferDate { get; set; }

        [StringLength(4000)]
        public string Message { get; set; }

        [StringLength(200)]
        public string Status { get; set; }

        public decimal? ClearanceAmount { get; set; }

        [StringLength(50)]
        public string EffectiveStartDate { get; set; }

        [StringLength(50)]
        public string EffectiveEndDate { get; set; }

        public string Description { get; set; }

        public string Remarks { get; set; }

        [StringLength(50)]
        public string ApprovedDateTime { get; set; }

        [StringLength(50)]
        public string ApprovedBy { get; set; }

        [StringLength(50)]
        public string CreateDateTime { get; set; }

        [StringLength(50)]
        public string CreateBy { get; set; }

        [StringLength(50)]
        public string UpdateDateTime { get; set; }

        [StringLength(50)]
        public string UpdateBy { get; set; }

        [StringLength(50)]
        public string StatusBeforeUpdate { get; set; }

        [StringLength(50)]
        public string ManualCheckDateTime { get; set; }

        public bool? Deleted { get; set; }

        public string Report { get; set; }

        [StringLength(50)]
        public string ProcessCode { get; set; }

        [StringLength(50)]
        public string DestinationMember { get; set; }

        public decimal? TotalSMSPoints { get; set; }

        [Required]
        [StringLength(400)]
        public string CartID { get; set; }

        [StringLength(50)]
        public string GatewayTxRemID { get; set; }

        [StringLength(255)]
        public string ShippingStreet { get; set; }

        [StringLength(255)]
        public string ShippingCity { get; set; }

        [StringLength(255)]
        public string ShippingState { get; set; }

        [StringLength(40)]
        public string ShippingPost { get; set; }

        [StringLength(100)]
        public string ShippingCountry { get; set; }

        [StringLength(40)]
        public string ShippingPhone { get; set; }

        [StringLength(255)]
        public string ShippingFirstName { get; set; }

        [StringLength(255)]
        public string ShippingLastName { get; set; }

        [StringLength(40)]
        public string POReferenceNo { get; set; }

        [StringLength(255)]
        public string BuyerEmailAddress { get; set; }

        [StringLength(40)]
        public string gatewayTransactionID { get; set; }

        [StringLength(20)]
        public string RESTORECODE { get; set; }

        [StringLength(400)]
        public string CallBackURL { get; set; }

        [StringLength(50)]
        public string LateStatus { get; set; }

        [StringLength(50)]
        public string LateStatusDateTime { get; set; }

        [StringLength(50)]
        public string LateStatusUpdateBy { get; set; }

        public string BrowserInfo { get; set; }

        [StringLength(50)]
        public string State { get; set; }

        [StringLength(128)]
        public string PostSign { get; set; }

        [StringLength(10)]
        public string BankSelection { get; set; }

        public decimal? SettlementFXRate { get; set; }

        public decimal? SettlementAmt { get; set; }

        [StringLength(50)]
        public string SettlementCur { get; set; }

        [StringLength(50)]
        public string SettlementBatchNumber { get; set; }

        [StringLength(50)]
        public string SettlementStatus { get; set; }

        [StringLength(50)]
        public string SettlementDateTime { get; set; }

        [StringLength(50)]
        public string msgToken { get; set; }

        [StringLength(50)]
        public string buyerBankId { get; set; }

        [StringLength(50)]
        public string buyerBankBranch { get; set; }

        [StringLength(50)]
        public string buyerAccNo { get; set; }

        [StringLength(50)]
        public string makerName { get; set; }

        [StringLength(50)]
        public string buyerIban { get; set; }

        public decimal? OriginalPrice { get; set; }

        [StringLength(3)]
        public string OriginalCurrency { get; set; }

        [StringLength(255)]
        public string MPIData { get; set; }

        [StringLength(50)]
        public string VoidBatchNumber { get; set; }

        [StringLength(255)]
        public string BillingDescriptor { get; set; }

        [StringLength(50)]
        public string CheckStatus { get; set; }

        [StringLength(255)]
        public string cartIDCheck { get; set; }

        [StringLength(400)]
        public string returnURL { get; set; }

        [StringLength(50)]
        public string defaultBankNumber { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? TipAmount { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? TipAmountAdjusted { get; set; }

        [StringLength(50)]
        public string TipAmountAdjustedStatus { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? TipAmountAdjustedLast { get; set; }

        [StringLength(50)]
        public string PreAuthRemId { get; set; }

        [StringLength(50)]
        public string GatewayService { get; set; }

        public decimal Agent1Amount { get; set; }

        public decimal Agent2Amount { get; set; }

        public decimal MDRAmount { get; set; }

        public decimal GatewayAmount { get; set; }

        [StringLength(50)]
        public string MerchantSettlementStatus { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? MerchantSettlementDateTime { get; set; }

        public string FrozenStatus { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? FrozenDateTime { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? UnfrozenDateTime { get; set; }

        [StringLength(255)]
        public string SerialNo { get; set; }

        [StringLength(255)]
        public string ResponseOrderNumber { get; set; }

        [StringLength(50)]
        public string DestNo { get; set; }

        [StringLength(50)]
        public string ProdCode { get; set; }

        [StringLength(50)]
        public string SourceNo { get; set; }

        [StringLength(50)]
        public string Deno { get; set; }

        [StringLength(50)]
        public string WalletCurrency { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? ProductPrice { get; set; }

        [StringLength(50)]
        public string AmountCurrency { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? AmountAfterTax { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Balance { get; set; }

        [StringLength(50)]
        public string ProductStatus { get; set; }

        [StringLength(4000)]
        public string ProductMessage { get; set; }

        [StringLength(50)]
        public string TerminalID { get; set; }

        public string UserID { get; set; }

        public virtual tPurchaseOrder tPurchaseOrders1 { get; set; }

        public virtual tPurchaseOrder tPurchaseOrder1 { get; set; }
    }
}
