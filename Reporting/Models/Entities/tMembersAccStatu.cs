namespace Reporting.Models.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tMembersAccStatu
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public decimal ID { get; set; }

        [StringLength(50)]
        public string CompanyRemID { get; set; }

        [StringLength(50)]
        public string MemberRemID { get; set; }

        [StringLength(50)]
        public string VerificationCode { get; set; }

        public bool? AccountVerified { get; set; }

        public decimal? LoginCount { get; set; }

        public bool? AccountLockout { get; set; }

        [StringLength(50)]
        public string LockoutDateTime { get; set; }

        [StringLength(50)]
        public string LockoutReleaseDateTime { get; set; }

        public bool? AccountSuspend { get; set; }

        [StringLength(50)]
        public string SuspendDateTime { get; set; }

        [StringLength(50)]
        public string ReleaseDateTime { get; set; }

        public bool? MonitorClosely { get; set; }

        public bool? AutoMaintain { get; set; }

        [StringLength(50)]
        public string CreateDateTime { get; set; }

        [StringLength(50)]
        public string CreateBy { get; set; }

        [StringLength(50)]
        public string UpdateDateTime { get; set; }

        [StringLength(50)]
        public string UpdateBy { get; set; }

        public bool? Deleted { get; set; }

        public bool? BankAccountVerified { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? BankAccountVerifyAmount { get; set; }

        public bool? BankAccountVerificationSent { get; set; }
    }
}
