namespace Reporting.Models.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tMembersAccInfo")]
    public partial class tMembersAccInfo
    {
        public int ID { get; set; }

        [StringLength(50)]
        public string CompanyRemID { get; set; }

        [StringLength(50)]
        public string MemberRemID { get; set; }

        [StringLength(50)]
        public string Rank { get; set; }

        [StringLength(50)]
        public string RankUpdateTime { get; set; }

        public decimal? ActiveDirectDwCount { get; set; }

        [Column(TypeName = "text")]
        public string SecretQuestion { get; set; }

        [Column(TypeName = "text")]
        public string SecretAnswer { get; set; }

        public bool? AutoReInvest { get; set; }

        public decimal? ReservationAmount { get; set; }

        public decimal? CurrentBalance { get; set; }

        public decimal? AvailableBalance { get; set; }

        public decimal? PWBalance { get; set; }

        public decimal? PurchasePVBalance { get; set; }

        public decimal? PVPoint { get; set; }

        public decimal? PVPointX { get; set; }

        public decimal? LeftPVPoint { get; set; }

        public decimal? MiddlePVPoint { get; set; }

        public decimal? RightPVPoint { get; set; }

        public decimal? LeftPVPointX { get; set; }

        public decimal? MiddlePVPointX { get; set; }

        public decimal? RightPVPointX { get; set; }

        public decimal? StockValue { get; set; }

        public decimal? LeftPVPointCF { get; set; }

        public decimal? MiddlePVPointCF { get; set; }

        public decimal? RightPVPointCF { get; set; }

        public decimal? LeftPVAdj { get; set; }

        public decimal? MiddlePVAdj { get; set; }

        public decimal? RightPVAdj { get; set; }

        public decimal? LeftPVPointCFY { get; set; }

        public decimal? MiddlePVPointCFY { get; set; }

        public decimal? RightPVPointCFY { get; set; }

        public decimal? LeftNewSales { get; set; }

        public decimal? MiddleNewSales { get; set; }

        public decimal? RightNewSales { get; set; }

        public decimal? AffiliateHits { get; set; }

        [StringLength(50)]
        public string TransDate { get; set; }

        [StringLength(50)]
        public string CreateDateTime { get; set; }

        [StringLength(50)]
        public string CreateBy { get; set; }

        [StringLength(50)]
        public string UpdateDateTime { get; set; }

        [StringLength(50)]
        public string UpdateBy { get; set; }

        public bool? Deleted { get; set; }

        public decimal? Bonus1 { get; set; }

        public decimal? Bonus2 { get; set; }

        public decimal? Bonus3 { get; set; }

        [StringLength(50)]
        public string QBonus1 { get; set; }

        [StringLength(50)]
        public string QBonus1UpdateTime { get; set; }

        [StringLength(50)]
        public string QBonus2 { get; set; }

        [StringLength(50)]
        public string QBonus2UpdateTime { get; set; }

        [StringLength(50)]
        public string QBonus3 { get; set; }

        [StringLength(50)]
        public string QBonus3UpdateTime { get; set; }

        [StringLength(50)]
        public string QBonus4 { get; set; }

        [StringLength(50)]
        public string QBonus4UpdateTime { get; set; }

        [StringLength(50)]
        public string QBonus5 { get; set; }

        public decimal? SMSWalletBalance { get; set; }

        public decimal? MaintWallet { get; set; }

        public decimal? ServiceFeeBalance { get; set; }

        [StringLength(50)]
        public string Currency { get; set; }
    }
}
