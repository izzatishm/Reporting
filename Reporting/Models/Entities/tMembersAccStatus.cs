namespace Reporting.Models.Entities
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class tMembersAccStatus : DbContext
    {
        public tMembersAccStatus()
            : base("name=tMembersAccStatus")
        {
        }

        public virtual DbSet<tMembersAccStatu> tMembersAccStatus1 { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<tMembersAccStatu>()
                .Property(e => e.ID)
                .HasPrecision(18, 0);

            modelBuilder.Entity<tMembersAccStatu>()
                .Property(e => e.LoginCount)
                .HasPrecision(18, 0);
        }
    }
}
