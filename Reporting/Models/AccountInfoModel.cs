﻿using Reporting.Models.Entities;
using Reporting.Models.Objects;
using Reporting.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Reporting.Models
{
    public class AccountInfoModel
    {
        public IEnumerable<AccountInfoTransactions> AccountInfo(AccountInfoViewModel filter)
        {
            using (var ctx = new ReportingContext())
            {
                var result = from A in ctx.tMembersInfoes
                             join B in ctx.tMembersAccStatus
                             on A.Site1CompanyRemID equals B.CompanyRemID
                             where A.CompanyRemID.Equals("M100-C-167")
                             select new
                             {
                                 A.CompanyRemID,
                                 A.CreateDateTime,
                                 A.Site1CompanyRemID,
                                 A.LoginUsername,
                                 B.AccountVerified
                             };

                if (filter.AccountInfoVM != null)
                {
                    if(filter.AccountInfoVM.Site1CompanyRemID != null)
                    {
                        result = result.Where(x => x.Site1CompanyRemID.Equals(filter.AccountInfoVM.Site1CompanyRemID));
                    }

                    if(filter.AccountInfoVM.AccountVerified != null)
                    {
                        result = result.Where(x => x.AccountVerified == (filter.AccountInfoVM.AccountVerified));
                    }
                }

                var FinalResult = from C in result
                                  select new AccountInfoTransactions
                                  {
                                      CreateDateTime = C.CreateDateTime,
                                      CompanyRemID = C.CompanyRemID,
                                      Site1CompanyRemID = C.Site1CompanyRemID,
                                      LoginUsername = C.LoginUsername,
                                      AccountVerified = C.AccountVerified
                                      
                                  };

                List < AccountInfoTransactions > MerchantAccountInfo = FinalResult.ToList();
                return MerchantAccountInfo;
            }
        }
    }
}