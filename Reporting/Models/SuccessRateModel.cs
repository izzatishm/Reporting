﻿using Reporting.Models.Entities;
using Reporting.Models.Objects;
using Reporting.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Reporting.Models
{
    public class SuccessRateModel
    {
        public IEnumerable<SuccessRateTransactions> SuccessRateTransactions(SuccessRateViewModel filter)
        {
            var StartDate = "2019-01-01 00:00:01";
            var EndDate = "2019-12-31 23:59:59";

            using (var ctx = new ReportingContext())
            {
                var result = from A in ctx.tPurchaseOrders
                             where A.TransferDate.CompareTo(StartDate) >= 0
                             && A.TransferDate.CompareTo(EndDate) < 0
                             select A;

                if (filter.successRate != null)
                {
                    if (filter.successRate.CompanyRemID != null)
                    {
                        result = result.Where(x => x.CompanyRemID.Equals(filter.successRate.CompanyRemID));
                    }

                    if (filter.successRate.StartDate != null || filter.successRate.EndDate != null)
                    {
                        result = result.Where(x => x.TransferDate.CompareTo(filter.successRate.StartDate) >= 0 && x.TransferDate.CompareTo(filter.successRate.EndDate) < 0);
                    }
                }

                var FinalResult = from A in result
                                  group A by new
                                  { 
                                      TransferDate1 = A.TransferDate.Substring(5, 2),
                                      TransferDate2 = A.TransferDate.Substring(8, 2) 
                                  } into g
                                  orderby g.Key ascending
                                  select new SuccessRateTransactions
                                  {
                                      Day = g.Key.TransferDate2,
                                      Month = g.Key.TransferDate1,
                                      SuccessRate = (decimal)100 * g.Count(x => x.Status.Substring(0,2).Equals("88")) / g.Count(),
                                      FailureRate = (decimal)100 * g.Count(x => x.Status.Substring(0, 2).Equals("66")) / g.Count(),
                                      TotalCount = g.Count()
                                  };

                List<SuccessRateTransactions> SummaryCounts = FinalResult.ToList();
                return SummaryCounts;
            }

        }


    }
}