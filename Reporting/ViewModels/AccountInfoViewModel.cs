﻿using Reporting.Models.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Reporting.ViewModels
{
    public class AccountInfoViewModel
    {
        public AccountInfoTransactions AccountInfoVM { get; set; }
        public IEnumerable<AccountInfoTransactions> ListAccountInfo { get; set; }
    }
}