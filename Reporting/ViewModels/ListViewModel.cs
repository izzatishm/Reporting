﻿using Reporting.Models.Entities;
using Reporting.Models.Objects;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Reporting.ViewModels
{
    public class ListViewModel
    {
        public ObjTransactions TransactionHistory { get; set; }
        public IEnumerable<tPurchaseOrder> TransactionList { get; set; }
      
    }
}