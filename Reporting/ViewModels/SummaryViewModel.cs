﻿using Reporting.Models.Entities;
using Reporting.Models.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Reporting.ViewModels
{
    public class SummaryViewModel
    {
        public SummaryTransactions Summary { get; set; }
        public IEnumerable<SummaryTransactions> ListSummary { get; set; }
    }
}