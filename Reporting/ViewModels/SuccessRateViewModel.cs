﻿using Reporting.Models.Entities;
using Reporting.Models.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Reporting.ViewModels
{
    public class SuccessRateViewModel
    {
        public SuccessRateTransactions successRate { get; set; }
        public IEnumerable<SuccessRateTransactions> ListSuccessRate { get; set; }
    }
}