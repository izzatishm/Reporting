﻿using Reporting.Models;
using Reporting.Models.Objects;
using Reporting.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Reporting.Controllers
{
    public class AccountInfoController : Controller
    {
        // GET: AccountInfo
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AccountInfo(AccountInfoViewModel filter)
        {
            AccountInfoModel accountInfoModel = new AccountInfoModel();
            IEnumerable<AccountInfoTransactions> accountInfoList = accountInfoModel.AccountInfo(filter);
            filter.ListAccountInfo = accountInfoList;
            return View(filter);
        }
    }
}