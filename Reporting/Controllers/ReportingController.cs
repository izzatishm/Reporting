﻿using Reporting.Models;
using Reporting.Models.Entities;
using Reporting.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Reporting.Controllers
{
    public class ReportingController : Controller
    {
        // GET: Reporting
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ListTransaction(ListViewModel filter)
        {
            //call the reporting Model
            ReportingModel reportingModel = new ReportingModel();
            //call the function inside the model
            IEnumerable<tPurchaseOrder> transactionList = reportingModel.ListTransaction(filter);
            //instance of ListViewModel
            filter.TransactionList = transactionList;
            //return the list
            return View(filter);
        }
    }
}