﻿using Reporting.Models;
using Reporting.Models.Objects;
using Reporting.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Reporting.Controllers
{
    public class SuccessRateController : Controller
    {
        // GET: SuccessRate
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SuccessRate(SuccessRateViewModel filter)
        {
            SuccessRateModel successRateModel = new SuccessRateModel();
            IEnumerable<SuccessRateTransactions> successRateList = successRateModel.SuccessRateTransactions(filter);
            filter.ListSuccessRate = successRateList;
            return View(filter);
        }
    }
}