﻿using Reporting.Models;
using Reporting.Models.Entities;
using Reporting.Models.Objects;
using Reporting.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Reporting.Controllers
{
    public class SummaryController : Controller
    {
        // GET: Summary
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SummaryTransactions(SummaryViewModel filter)
        {
            SummaryModel summaryModel = new SummaryModel();
            IEnumerable<SummaryTransactions> summaryList = summaryModel.SummaryTransactions(filter);
            filter.ListSummary = summaryList;
            return View(filter);
        }
    }
}